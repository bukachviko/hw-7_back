import {Request, Response} from "express";
import {FileDB} from "../repositories/database.service";
import {errorHandler} from "./baseController";
import {NewsPostsService} from "../services/NewsPostsService";
import {NewsPost} from "../interfaces/NewsPost.interface";

export const FILEDB = 'fileDB.json';

export const getAllNewsPosts = async (req: Request, res: Response) => {
    try {
        const newsPostRepository = await FileDB.getNewsPostRepository(FILEDB);
        const newsPostService = new NewsPostsService(newsPostRepository);
        const page = parseInt(req.query.page + '');
        const size = parseInt(req.query.size + '')

        let tableData: NewsPost[] = [];
        if(page && size) {
            tableData = await newsPostService.getAll({page: page, size: size});
        } else {
            tableData = await newsPostService.getAll(null);
        }

        const totalPosts = await newsPostService.getAll(null);


        res.send({
            newsPosts: tableData,
            currentPage: page,
            totalPosts: totalPosts.length,
            size: size,
        })
    } catch (err) {
        console.log('Error:', err);
        errorHandler(err, req, res);
    }
};


export const getNewsPostById = async (req: Request, res: Response) => {
    const newsId: number = parseInt(req.params.id);
    try {
        const newsPostRepository = await FileDB.getNewsPostRepository(FILEDB);
        const newsPostService = new NewsPostsService(newsPostRepository);
        const tableData = await newsPostService.getById(newsId);

        if (!tableData) {
            res.status(404).send('News not found');
            return;
        }
        res.send(tableData);
    } catch (err) {
        console.log('Error:', err);
        errorHandler(err, req, res);
    }
};

export const createNewsPost = async (req: Request, res: Response) => {
    const formData = req.body;
    try {
        const newsPostRepository = await FileDB.getNewsPostRepository(FILEDB);
        const newsPostService = new NewsPostsService(newsPostRepository);
        const tableData = await newsPostService.create(formData)
        res.send(tableData)
    } catch (err) {
        console.log('Error', err);
        errorHandler(err, req, res);
    }
};

export const updateNewsPost = async (req: Request, res: Response) => {
    const newsId: number = parseInt(req.params.id);
    const formData = req.body;
    try {
        const newsPostRepository = await FileDB.getNewsPostRepository(FILEDB);
        const newsPostService = new NewsPostsService(newsPostRepository);
        const tableData = await newsPostService.update(newsId, formData);

        if (!tableData) {
            res.status(404).send('News not found');
            return;
        }
        res.send(tableData)
    } catch (err) {
        console.log('Error:', err);
        errorHandler(err, req, res);
    }
};

export const deleteNewsPost = async (req: Request, res: Response) => {
    const newsId: number = parseInt(req.params.id);
    try {
        const newsPostRepository = await FileDB.getNewsPostRepository(FILEDB);
        const newsPostService = new NewsPostsService(newsPostRepository)
        const deletedNews = await newsPostService.delete(newsId);
        if (!deletedNews) {
            res.status(404).send('News not found');
            return;
        }
        res.status(200).send('News deleted successfully');
    } catch (err) {
        console.log('Error:', err);
        errorHandler(err, req, res);
    }
};
