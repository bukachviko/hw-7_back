import { NewsPost } from "../interfaces/NewsPost.interface";
import {NewsPostRepository} from "../repositories/NewsPostRepository";

export class NewsPostsService {
    private newsPostRepository: NewsPostRepository;

    constructor(newsPostRepository: NewsPostRepository) {
        this.newsPostRepository = newsPostRepository;
    }

    async getAll(params: { page: number, size: number } | null ):Promise<NewsPost[]> {
        if( params) {
            return await this.newsPostRepository.getAll(params);
        }
        return await this.newsPostRepository.getAll(null);
    };

    async getById(newsId: number):Promise<NewsPost | null> {
        return await this.newsPostRepository.getById(newsId);
    };

    async create(newsPostData: { title: string; text:string }): Promise<NewsPost | void> {
        return await this.newsPostRepository.create(newsPostData);
    };

    async update(id: number, newData: { title?: string, text?: string }): Promise<NewsPost | void> {
        return await this.newsPostRepository.update(id, newData);
    };

    async delete(id: number): Promise<number | void> {
        return await this.newsPostRepository.delete(id);
    };
}





