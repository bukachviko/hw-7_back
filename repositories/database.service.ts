import fsPromise from 'fs/promises';
import path from 'path';
import { TableData } from "../interfaces/TableData.interface";
import { NewsPostSchema } from "../schemas/newPostSchema";
import { NewsPostRepository } from "./NewsPostRepository";

export class FileDB {
    static async RegisterSchema(fileName: string, schema: Record<string, string>): Promise<void> {
        try {
            const data: TableData = {
                table: [],
                id: 1,
                schema: schema,
            };

            await fsPromise.writeFile(fileName, JSON.stringify(data, null, 2));
            console.log(`Schema registered for ${fileName}`)
        } catch (err) {
            console.error(`Error creating file ${fileName}:`, err);
        }
    };

    static async getNewsPostRepository(tableName: string): Promise<NewsPostRepository> {

        const filePath = path.join(__dirname, '..', 'db', tableName);
        try {
            await fsPromise.readFile(filePath, 'utf-8');
        } catch (err) {
            await this.RegisterSchema(filePath, NewsPostSchema);
        }
        return new NewsPostRepository(filePath, NewsPostSchema);
    };
}



